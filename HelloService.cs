﻿using CsvHelper;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WCF2
{
    
    public class HelloService : IHelloService
    {

        public Movie AddMovie(string Title, string Genre, string Director, int Rating)
        {
            Movie movie = new Movie
            {
                Title = Title,
                Genre = Genre,
                Director = Director,
                Rating = Rating
            };

            var streamReader = File.OpenText("Movies.csv");
            var reader = new CsvReader(streamReader);

            List<Movie> moviesList = reader.GetRecords<Movie>().ToList();
            moviesList.Add(movie);

            //streamReader.Close();

            var writer = new StreamWriter("Movies1.csv");
            var csv = new CsvWriter(writer);

            csv.WriteRecords(moviesList);

            return movie;
        }

        public List<Movie> GetMovieList()
        {
            using (var streamReader = File.OpenText("Movies.csv"))
            {
                var reader = new CsvReader(streamReader);
                List<Movie> movies = reader.GetRecords<Movie>().ToList();
                return movies;
            }
        }

    }
}
