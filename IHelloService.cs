﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ServiceModel;

namespace WCF2
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IHelloService
    {

        [OperationContract]
        Movie AddMovie(string Title, string Genre, string Director, int Rating);

        [OperationContract]
        List<Movie> GetMovieList();   
    }
}
